#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include "es.hpp"

int main()
{
    // Create the main window
    sf::RenderWindow App(sf::VideoMode(800, 600), "SFML window");

    sf::Music Music;
    Music.OpenFromFile("data/Wrathchild.ogg");
    Music.SetVolume(16);
    Music.Play();

    sf::SoundBuffer Buffer;
    Buffer.LoadFromFile("data/step.wav");
    sf::Sound Step;
    Step.SetBuffer(Buffer); // Buffer is a sf::SoundBuffer
    Step.SetVolume(24);

    // Load a sprite to display
    sf::Image Image;
    if (!Image.LoadFromFile("data/terra.jpg"))
        return EXIT_FAILURE;
    sf::Sprite Sprite(Image);

    sf::Image Image3;
    if (!Image3.LoadFromFile("data/terra2.jpg"))
        return EXIT_FAILURE;
    sf::Sprite Sprite3(Image3);

    sf::Image Image4;
    if (!Image4.LoadFromFile("data/terra2.jpg"))
        return EXIT_FAILURE;
    sf::Sprite Sprite4(Image4);
    for (int x=0; x<800; x++)
    {
        for (int y=0; y<600; y++)
        {
            Image4.SetPixel(x,y,sf::Color(0,0,0,240));
        }
    }
    sf::Image Image3_copy(Image3);
    sf::Image Image4_copy(Image4);

    // Load a sprite to display
    sf::Image Image2;
    if (!Image2.LoadFromFile("data/Miner.png"))
        return EXIT_FAILURE;
    sf::Sprite Sprite2(Image2,sf::Vector2f(0,0),sf::Vector2f(0.5,0.5));
    Sprite2.SetSubRect(sf::IntRect(0, 20, 50, 90));
    sf::IntRect anim[10];
    anim[0]=sf::IntRect(0, 20, 51, 90);
    anim[1]=sf::IntRect(51, 20, 101, 90);
    anim[2]=sf::IntRect(101, 20, 151, 90);
    anim[3]=sf::IntRect(151, 20, 201, 90);
    anim[4]=sf::IntRect(201, 20, 251, 90);
    anim[5]=sf::IntRect(251, 20, 301, 90);
    anim[6]=sf::IntRect(301, 20, 341, 90);
    anim[7]=sf::IntRect(341, 20, 383, 90);
    anim[8]=sf::IntRect(383, 20, 440, 90);
    anim[9]=sf::IntRect(440, 20, 490, 90);

    sf::Vector2f animpos[10];
    animpos[0]=sf::Vector2f(0,0);
    animpos[1]=sf::Vector2f(0,0);
    animpos[2]=sf::Vector2f(0,0);
    animpos[3]=sf::Vector2f(0,0);
    animpos[4]=sf::Vector2f(0,0);
    animpos[5]=sf::Vector2f(0,0);
    animpos[6]=sf::Vector2f(5,0);
    animpos[7]=sf::Vector2f(5,0);
    animpos[8]=sf::Vector2f(6,0);
    animpos[9]=sf::Vector2f(9,0);

    sf::Vector2f animpos2[10];
    animpos2[0]=sf::Vector2f(5,0);
    animpos2[1]=sf::Vector2f(5,0);
    animpos2[2]=sf::Vector2f(5,0);
    animpos2[3]=sf::Vector2f(5,0);
    animpos2[4]=sf::Vector2f(5,0);
    animpos2[5]=sf::Vector2f(5,0);
    animpos2[6]=sf::Vector2f(5,0);
    animpos2[7]=sf::Vector2f(4,0);
    animpos2[8]=sf::Vector2f(-4,0);
    animpos2[9]=sf::Vector2f(-4,0);

    float anim1 = 0;

    bool passou = false;

    sf::Image ImagePedra;
    if (!ImagePedra.LoadFromFile("data/pedra.png"))
        return EXIT_FAILURE;
    sf::Sprite SpritePedra(ImagePedra,sf::Vector2f(0,0),sf::Vector2f(0.5,0.5));
    SpritePedra.SetSubRect(sf::IntRect(0, 0, 63, 63));
    for (int x=0; x<ImagePedra.GetWidth(); x++)
    {
        for (int y=0; y<ImagePedra.GetHeight(); y++)
        {
            if (ImagePedra.GetPixel(x,y)==sf::Color(255,0,255))
                ImagePedra.SetPixel(x,y,sf::Color(0,0,0,0));
        }
    }

    sf::Image ImageSaida;
    if (!ImageSaida.LoadFromFile("data/pedra.png"))
        return EXIT_FAILURE;
    sf::Sprite SpriteSaida(ImageSaida);
    SpriteSaida.SetSubRect(sf::IntRect(0, 0, 63, 63));


    Sprite2.SetPosition(0,0);
    int direcao = 0;
    sf::Vector2f posicao(35,35);
    sf::Vector2f ultima_posicao(35,35);
    int Speed = 100;
    bool direita = false, esquerda = false, cima = false, baixo = false;
    std::deque<sf::Sprite> pedras;
    std::deque<sf::Vector2f> deque_saida;
    bool colide = false;

    sf::Font MyFont;
    if (!MyFont.LoadFromFile("arial.ttf", 12))
    {
        // Error...
    }
    sf::String Text("Voc� ficou preso em um desmoronamento, cave at� a sa�da.", MyFont, 12);
    sf::String TextPassou("Parab�ns, voc� conseguiu sair da mina!", MyFont, 12);
    sf::String TextPassou2("Aguarde 10 segundos para voltar a minerar ou pressione ESC para se aposentar.", MyFont, 12);



    ///Mapa
    CES *Arquivo = new CES();
    Arquivo->AbreArquivo("sfml-core.dll","E");
    int X=0,Y=0;
    for (int a = 0; a < Arquivo->BufferSize(); a++)
    {
        for (int b = 0; b < Arquivo->Buffer(a).size(); b++)
        {
            if (Arquivo->Buffer(a)[b] != ',')
            {
                if (atoi(&Arquivo->Buffer(a)[b])==1)
                {
                    SpritePedra.SetPosition(30*X,30*Y);
                    pedras.push_back(SpritePedra);
                }
                else if (atoi(&Arquivo->Buffer(a)[b])==2)
                {
                    deque_saida.push_back(sf::Vector2f(30*X,30*Y));
                }
            }
            else
            {
                X++;
            }
        }
        X=0;
        Y++;
    }

    //Saida
    int posSaida = rand()%deque_saida.size();
    for (int x=0; x<ImageSaida.GetWidth(); x++)
    {
        for (int y=0; y<ImageSaida.GetHeight(); y++)
        {
            int distanciax = x-15;
            distanciax *= distanciax;
            int distanciay = y-15;
            distanciay *= distanciay;
            if (sqrt(distanciax+distanciay)<7)
                ImageSaida.SetPixel(x,y,sf::Color(0,0,0,255));
            else
                ImageSaida.SetPixel(x,y,sf::Color(0,0,0,0));
        }
    }
    SpriteSaida.SetPosition(deque_saida[posSaida]);


    for (int x=deque_saida[posSaida].x-50; x<deque_saida[posSaida].x+50; x++)
    {
        for (int y=deque_saida[posSaida].y-50; y<deque_saida[posSaida].y+50; y++)
        {
            int distanciax = x-deque_saida[posSaida].x-15;
            distanciax *= distanciax;
            int distanciay = y-deque_saida[posSaida].y-15;
            distanciay *= distanciay;
            if (sqrt(distanciax+distanciay)<25)
                Image4.SetPixel(x,y,sf::Color(0,0,0,220*sqrt(distanciax+distanciay)/25));
        }
    }

    sf::String TextSaida("Sa�da", MyFont, 12);
    TextSaida.Move(deque_saida[posSaida].x-3,deque_saida[posSaida].y-10);

    float tempo_inicio=0;
    float tempo_som=0;
    // Start the game loop
    while (App.IsOpened())
    {
        // Process events
        sf::Event Event;
        while (App.GetEvent(Event))
        {
            // Close window : exit
            if (Event.Type == sf::Event::Closed)
                App.Close();
            if (Event.Type == sf::Event::KeyPressed)
            {
                if (Event.Key.Code == sf::Key::Right)
                {
                    direita = true;
                }
                if (Event.Key.Code == sf::Key::Left)
                {
                    esquerda = true;
                }
                if (Event.Key.Code == sf::Key::Down)
                {
                    baixo = true;
                }
                if (Event.Key.Code == sf::Key::Up)
                {
                    cima = true;
                }
                if (Event.Key.Code == sf::Key::Escape)
                {
                    if (passou)
                        App.Close();
                }
            }
            if (Event.Type == sf::Event::KeyReleased)
            {
                if (Event.Key.Code == sf::Key::Right)
                {
                    direita = false;
                }
                if (Event.Key.Code == sf::Key::Left)
                {
                    esquerda = false;
                }
                if (Event.Key.Code == sf::Key::Down)
                {
                    baixo = false;
                }
                if (Event.Key.Code == sf::Key::Up)
                {
                    cima = false;
                }
            }

        }

        // Clear screen
        App.Clear();

        if (!passou)
        {
            tempo_inicio=0;

            if(ultima_posicao!=posicao)
            {
                tempo_som+=2*App.GetFrameTime();
                if(tempo_som>1)
                {
                    Step.Play();
                    tempo_som-=1;
                }
                else
                {
                    Step.Stop();
                }

                ultima_posicao=posicao;
            }
            else
            {
                tempo_som=1;
                Step.Stop();
            }

            if (direita)
            {
                direcao = 0;
                for (int c=0; c<pedras.size()&&!colide; c++)
                {
                    if (pedras[c].GetPosition().x<posicao.x+Sprite2.GetSize().x+5
                            &&pedras[c].GetPosition().x+30>posicao.x+Sprite2.GetSize().x
                            &&pedras[c].GetPosition().y<posicao.y+Sprite2.GetSize().y-10
                            &&pedras[c].GetPosition().y+30>posicao.y)
                        colide = true;
                }
                if (!colide&&posicao.x<800)
                    posicao.x+=Speed * App.GetFrameTime();
                colide = false;
            }
            if (esquerda)
            {
                direcao = 1;
                for (int c=0; c<pedras.size()&&!colide; c++)
                {
                    if (pedras[c].GetPosition().x<posicao.x
                            &&pedras[c].GetPosition().x+30>posicao.x-5
                            &&pedras[c].GetPosition().y<posicao.y+Sprite2.GetSize().y-10
                            &&pedras[c].GetPosition().y+30>posicao.y)
                        colide = true;
                }
                if (!colide&&posicao.x>0)
                    posicao.x-=Speed * App.GetFrameTime();
                colide = false;
            }
            if (baixo)
            {
                for (int c=0; c<pedras.size()&&!colide; c++)
                {
                    if (pedras[c].GetPosition().x<posicao.x+Sprite2.GetSize().x
                            &&pedras[c].GetPosition().x+30>posicao.x
                            &&pedras[c].GetPosition().y<posicao.y+Sprite2.GetSize().y-5
                            &&pedras[c].GetPosition().y+30>posicao.y+Sprite2.GetSize().y-10)
                        colide = true;
                }
                if (!colide&&posicao.y<600)
                    posicao.y+=Speed * App.GetFrameTime();
                colide = false;
            }
            if (cima)
            {
                for (int c=0; c<pedras.size()&&!colide; c++)
                {
                    if (pedras[c].GetPosition().x<posicao.x+Sprite2.GetSize().x
                            &&pedras[c].GetPosition().x+30>posicao.x
                            &&pedras[c].GetPosition().y<posicao.y
                            &&pedras[c].GetPosition().y+30>posicao.y-5)
                        colide = true;
                }
                if (!colide&&posicao.y>0)
                    posicao.y-=Speed * App.GetFrameTime();
                colide = false;
            }


            if (direcao==0)
            {
                Sprite2.FlipX(1);
                Sprite2.SetPosition(animpos2[(int)anim1]-sf::Vector2f(0,10));
            }
            if (direcao==1)
            {
                Sprite2.FlipX(0);
                Sprite2.SetPosition(animpos[(int)anim1]-sf::Vector2f(0,10));
            }
            Sprite2.SetSubRect(anim[(int)anim1]);
            Sprite2.SetPosition(Sprite2.GetPosition()+posicao);
            anim1+=20*App.GetFrameTime();
            if (anim1>9)
                anim1=0;

            for (int x=posicao.x-10; x<Sprite2.GetSize().x+posicao.x+10; x++)
            {
                for (int y=posicao.y-10; y<Sprite2.GetSize().y+posicao.y; y++)
                {
                    int distanciax = x-(posicao.x+Sprite2.GetSize().x/2);
                    distanciax *= distanciax;
                    int distanciay = y-(posicao.y-5+Sprite2.GetSize().y/2);
                    distanciay *= distanciay;
                    if (sqrt(distanciax+distanciay)<20)
                        Image3.SetPixel(x,y,sf::Color(0,0,0,0));
                }
            }

            for (int x=posicao.x-50; x<Sprite2.GetSize().x+posicao.x+50; x++)
            {
                for (int y=posicao.y-50; y<Sprite2.GetSize().y+posicao.y+40; y++)
                {
                    int distanciax = x-(posicao.x+11);
                    distanciax *= distanciax;
                    int distanciay = y-(posicao.y+14);
                    distanciay *= distanciay;
                    if (sqrt(distanciax+distanciay)<35)
                        Image4.SetPixel(x,y,sf::Color(0,0,0,220*sqrt(distanciax+distanciay)/35));
                    else if (sqrt(distanciax+distanciay)<50)
                    {
                        Image4.SetPixel(x,y,sf::Color(0,0,0,220));
                    }
                }
            }

            ///Luz saida
            for (int x=deque_saida[posSaida].x-50; x<deque_saida[posSaida].x+50; x++)
            {
                for (int y=deque_saida[posSaida].y-50; y<deque_saida[posSaida].y+50; y++)
                {
                    int distanciax = x-deque_saida[posSaida].x-15;
                    distanciax *= distanciax;
                    int distanciay = y-deque_saida[posSaida].y-15;
                    distanciay *= distanciay;
                    if (sqrt(distanciax+distanciay)<25)
                        Image4.SetPixel(x,y,sf::Color(0,0,0,220*sqrt(distanciax+distanciay)/25));
                }
            }

            TextSaida.SetPosition(deque_saida[posSaida].x-3,deque_saida[posSaida].y-10);


            // Draw the sprite
            App.Draw(Sprite);

            for (int c=0; c<pedras.size(); c++)
                App.Draw(pedras[c]);

            App.Draw(Sprite2);

            App.Draw(Sprite3);

            App.Draw(SpriteSaida);

            App.Draw(Sprite4);

            App.Draw(Text);
            App.Draw(TextSaida);

        }
        else
        {
            Music.SetVolume(32);

            ///Reseta a posi��o
            posicao.x=35;
            posicao.y=35;

            ///Reseta a areia
            Image3.Copy(Image3_copy,0,0);
            Image4.Copy(Image4_copy,0,0);

            ///Saida
            posSaida = rand()%deque_saida.size();
            SpriteSaida.SetPosition(deque_saida[posSaida]);
            for (int x=deque_saida[posSaida].x-50; x<deque_saida[posSaida].x+50; x++)
            {
                for (int y=deque_saida[posSaida].y-50; y<deque_saida[posSaida].y+50; y++)
                {
                    int distanciax = x-deque_saida[posSaida].x-15;
                    distanciax *= distanciax;
                    int distanciay = y-deque_saida[posSaida].y-15;
                    distanciay *= distanciay;
                    if (sqrt(distanciax+distanciay)<25)
                        Image4.SetPixel(x,y,sf::Color(0,0,0,220*sqrt(distanciax+distanciay)/25));
                }
            }

            // Draw the sprite
            App.Draw(Sprite2);

            App.Draw(Sprite3);

            App.Draw(SpriteSaida);

            App.Draw(Sprite4);

            TextPassou.SetPosition(50,200);
            TextPassou2.SetPosition(50,250);

            App.Draw(TextPassou);
            App.Draw(TextPassou2);

            tempo_inicio+=App.GetFrameTime();
            char Passou2[100];
            sprintf(Passou2,"Aguarde %.0f segundos para voltar a minerar ou pressione ESC para se aposentar.",10.0f-tempo_inicio);
            TextPassou2.SetText(Passou2);


            if (tempo_inicio>10)
            {
                passou = false;
                Music.SetVolume(16);
                Music.Stop();
                Music.Play();

            }
        }

        if (posicao.x>deque_saida[posSaida].x&&posicao.x<deque_saida[posSaida].x+30
                &&posicao.y>deque_saida[posSaida].y&&posicao.y<deque_saida[posSaida].y+30)
        {
            passou = true;
        }


        // Update the window
        App.Display();
    }

    return EXIT_SUCCESS;
}
