/**
* ==========================================================
* Projeto Arret 0.0.9a
* http:\\www.projetoarret.com.br
* ==========================================================
* Desenvolvido por: Alexandro Trevisan
                    Fernando Tonon de Rossi
* Data cria��o: 24/05/2007, 13:29
* Data atualiza��o: 31/05/2007, 23:10
* Atualizado por: Alexandro Trevisan
**/

#include "es.hpp"


void CES::AbreArquivo(std::string arquivo, std::string tipo)
{
    //if(!this->_Aberto)
    //{
        _Linha = 0;
        if(tipo == "E")
        {
            this->_E_Arquivo.open(arquivo.c_str());
            if(!this->_E_Arquivo.is_open())
            {
                this->_Aberto = false;
                printf("Arquivo \"%s\" nao encontrado!\n", arquivo.c_str());
            }
            else
            {
                //_E_Arquivo.seekg(0);
                char buffer[100];
                while(!this->_E_Arquivo.eof())
                {
                    this->_E_Arquivo.getline(buffer,100);
                    _Buffer.push_back(buffer);
                }
                this->_Aberto = true;
            }
        }
        else if(tipo == "S")
        {
            this->_E_Arquivo.open(arquivo.c_str());
            if(!this->_E_Arquivo.is_open())
            {
                printf("Arquivo \"%s\" nao encontrado!\n", arquivo.c_str());
            }
            else
            {
                //printf("o arquivo abriu\n");
                //this->_S_Arquivo.write("a",1);

                while(!this->_E_Arquivo.eof())
                {
                    char buffer[100];
                    this->_E_Arquivo.getline(buffer,100);
                    //printf("%s\n",buffer);
                    _Buffer.push_back(buffer);
                }
                /*for(int a = 0; a< Buffer.size(); a++)
                {
                    //printf("%s\n", Buffer[a].c_str());
                }*/
                this->_E_Arquivo.close();
                this->_S_Arquivo.open(arquivo.c_str());
            }
        }
    //}
    //else
    //{
     //   printf("Ja existe arquivo aberto!Crie outra sessao\n");
    //}
}


void CES::CarregaArquivo()
{
    /*
    if(Buffer.size()<=0)
    {
        _E_Arquivo.seekg(0);
        char buffer[100];
        while(!this->_E_Arquivo.eof())
        {
            this->_E_Arquivo.getline(buffer,100);
            Buffer.push_back(buffer);
        }
    }
    else
    {
        printf("Buffer ja criado!\n");
    }
    */
}
void CES::FechaArquivo()
{
    if(this->_E_Arquivo.is_open())
    {
        this->_E_Arquivo.close();
        this->_Aberto = false;
        while(_Buffer.size()>0)
        {
            _Buffer.pop_back();
        }
    }
    //printf("%s\n",Buffer[0]);
    if(this->_S_Arquivo.is_open())
    {
        //printf("%s\n",Buffer[0]);
        for(int TamBuffer = 0; TamBuffer < _Buffer.size(); TamBuffer++)
        {
            _S_Arquivo<<_Buffer[TamBuffer]<<std::endl;
        }
        this->_S_Arquivo.close();
        this->_Aberto = false;
        _Buffer.clear();
    }
}

void CES::PegaProximaLinha(std::string& linha)
{
    char _Buffer[100];
    if(!this->_E_Arquivo.eof())
    {
        this->_E_Arquivo.getline(_Buffer,100);
    }
    linha = _Buffer;
}
void CES::Escreve(std::string escreve)
{
    escreve = escreve + "\n";
    if(_Buffer.size()>_Linha)
    {
        _Buffer[_Linha] = escreve;
        _Linha++;
    }
    else
    {
        _Buffer.push_back(escreve);
        _Linha++;
    }
}

void CES::Escreve(std::string escreve, int linha)
{
    escreve = escreve;
    if(linha <= 0)
    {
        linha = 1;
    }
    if(_Buffer.size()>linha-1)
    {
        _Buffer[linha-1] = escreve;
    }
    else
    {
        while(_Buffer.size()<linha-1)
        {
            _Buffer.push_back("\n");
        }
        _Buffer.push_back(escreve);
    }
}



bool CES::Aberto()
{
    return this->_Aberto;
}

std::string& CES::Buffer(int indice)
{
    if(indice>=_Buffer.size())
    {
        printf("Valor do buffer estourado CES::BUFFER\n");
    }
    return _Buffer[indice];
}

std::string& CES::Buffer(int indicex,int indicey)
{
    if(indicex>=_Buffer.size())
    {
        printf("Valor do buffer estourado CES::BUFFER\n");
    }
    return _Buffer[indicex];
}

int CES::BufferSize()
{
    return _Buffer.size();
}
CES::CES()
{

}
CES::~CES()
{
    _Aberto = false;
    _Entrada = false;
    _Linha = 0;
    if(_E_Arquivo.is_open()||_S_Arquivo.is_open())
    {
        FechaArquivo();
    }
    _Buffer.clear();
    _Erro.clear();
}
