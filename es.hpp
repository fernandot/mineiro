/**
* ==========================================================
* Projeto Arret 0.0.9a
* http:\\www.projetoarret.com.br
* ==========================================================
* Desenvolvido por: Alexandro Trevisan
                    Fernando Tonon de Rossi
* Data cria��o: 24/05/2007, 13:29
* Data atualiza��o: 31/05/2007, 23:10
* Atualizado por: Alexandro Trevisan
**/

#ifndef _es_hpp_
#define _es_hpp_

#include <fstream>
#include <string>
#include <vector>
#include <cstdio>
#include <iostream>

class CES
{
    private:
        std::ifstream _E_Arquivo;
        std::ofstream  _S_Arquivo;
        bool _Aberto;
        bool _Entrada;
        int _Linha;
        std::string _Erro;
        std::vector<std::string> _Buffer;
    public:
        CES();
        ~CES();
        void AbreArquivo(std::string arquivo, std::string tipo);
        void CarregaArquivo();
        std::string& Procura(std::string temp);
        void FechaArquivo();
        void PegaProximaLinha(std::string& linha);
        void Escreve(std::string escreve);
        void Escreve(std::string escreve, int linha);
        int ProcuraLinha(std::string temp);
        bool Aberto();
        std::string& Buffer(int indice);
        std::string& Buffer(int indicex,int indicey);

        int BufferSize();

};


#endif
